package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.Item;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadServices( );
					System.out.println("Carga exitosa\n");
					break;
				case 2:
					System.out.println("Ingrese el nombre de la compa�ia (Si desea buscar taxis independientes ingrese Independientes):");
					String companyName = new Scanner(System.in).nextLine();
					LinkedList<Taxi> taxiList = Controller.getTaxisOfCompany(companyName);
					
					Node nodoAct = taxiList.getFirst();					
					while(nodoAct != null)
					{
						System.out.println("-> " + ((Taxi)nodoAct.getItem()).getTaxiId());
						nodoAct = nodoAct.getNext();
					}
					System.out.println("\nSe encontraron "+ taxiList.size() + " taxis afiliados a la empresa " + companyName + "\n");
					taxiList = null;
					
					break;
				case 3:
					System.out.println("Ingrese el identificador de la comunidad");
					int communityId = Integer.parseInt(sc.next());
					LinkedList<Service> taxiServicesList = Controller.getTaxiServicesToCommunityArea(communityId);
					
					Node nodoAc = taxiServicesList.getFirst();					
					while(nodoAc != null)
					{
						System.out.println("-> Taxi ID: " + ((Service)nodoAc.getItem()).getTaxiId() + "\n   (Trip ID: " + ((Service)nodoAc.getItem()).getTripId() + ")\n   (" + ((Service)nodoAc.getItem()).getTripSeconds() + " seg) - (" + ((Service)nodoAc.getItem()).getTripMiles() + " miles) - ($" + ((Service)nodoAc.getItem()).getTripTotal() + ")");
						nodoAc = nodoAc.getNext();
					}
					System.out.println("\nSe encontraron " + taxiServicesList.size() + " servicios terminados en el area " + communityId + "\n");
					taxiServicesList = null;
					
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
					
				default: System.out.println("N�mero errado");;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cargar un subconjunto de datos de servicios de taxis");
		System.out.println("2. Dar lista de taxis de una compa�ia");
		System.out.println("3. Dar listado de servicios que finalizan en un �rea espec�fica de la ciudad");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
