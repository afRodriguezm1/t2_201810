package model.data_structures;

import java.awt.Point;
import java.util.Date;

public class Item 
{
	private String trip_id;

	private String taxi_id;
	
	private Date trip_start_timestamp;

	private Date trip_end_timestamp;

	private int trip_seconds;

	private float trip_miles;

	private String pickup_census_tract;

	private String dropoff_census_tract;

	private int pickup_community_area;

	private int dropoff_community_area;
	
	private float fare;
	
	private float tips;
	
	private float tolls;
	
	private float extras;
	
	private float trip_total;

	private String payment_type;
	
	private String company;
	
	private double pickup_centroid_latitude;

	private double pickup_centroid_longitude;

	private Point pickup_centroid_location;

	private double dropoff_centroid_latitude;

	private double dropoff_centroid_longitude;

	private Point dropoff_centroidL_location;

	public String getTrip_id() 
	{
		return trip_id;
	}

	public String getTaxi_id() 
	{
		return taxi_id;
	}

	public Date getTrip_start_timestamp() 
	{
		return trip_start_timestamp;
	}

	public Date getTrip_end_timestamp() 
	{
		return trip_end_timestamp;
	}

	public int getTrip_seconds() 
	{
		return trip_seconds;
	}

	public float getTrip_miles() 
	{
		return trip_miles;
	}

	public String getPickup_census_tract() 
	{
		return pickup_census_tract;
	}

	public String getDropoff_census_tract() 
	{
		return dropoff_census_tract;
	}

	public int getPickup_community_area() 
	{
		return pickup_community_area;
	}

	public int getDropoff_community_area() 
	{
		return dropoff_community_area;
	}

	public float getFare() 
	{
		return fare;
	}

	public float getTips() 
	{
		return tips;
	}

	public float getTolls() 
	{
		return tolls;
	}

	public float getExtras() 
	{
		return extras;
	}

	public float getTrip_total() 
	{
		return trip_total;
	}

	public String getPayment_type() 
	{
		return payment_type;
	}

	public String getCompany() 
	{
		return company;
	}

	public double getPickup_centroid_latitude() 
	{
		return pickup_centroid_latitude;
	}

	public double getPickup_centroid_longitude() 
	{
		return pickup_centroid_longitude;
	}

	public Point getPickup_centroid_location() 
	{
		return pickup_centroid_location;
	}

	public double getDropoff_centroid_latitude() 
	{
		return dropoff_centroid_latitude;
	}

	public double getDropoff_centroid_longitude() 
	{
		return dropoff_centroid_longitude;
	}

	public Point getDropoff_centroidL_location() 
	{
		return dropoff_centroidL_location;
	}

	
}
