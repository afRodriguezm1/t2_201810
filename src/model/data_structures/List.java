package model.data_structures;

import model.vo.Taxi;

public class List <T extends Comparable<T>> implements LinkedList<T>
{
	private Node<T> head;
	private Node<T> tail;
	private int size;
	
	public void add(Node node)
	{			

		if(head == null)
		{
			head = node;
			tail = node;
			size ++;
		}
		else
		{
			tail.setNextNode(node);
			tail = node;
			size++;
		}
	}
	
	public Node<T> get(int pIndex)
	{
		if(head == null)
			return null;
		else
		{
			int pos = 0;
			Node<T> nodo = head;
			while(nodo.getNext()!= null && pos <= pIndex )
			{
				nodo = nodo.getNext();
				pos ++;
			}
			return nodo;
		}
	}
	
	public void addTaxi(Node node)
	{
		if(head == null)
		{
			head = node;
			tail = node;
			size ++;
		}
		else
		{
			Taxi newTaxi = (Taxi)node.getItem();
			Node nodeAct = head;
			while(nodeAct != null)
			{
				if(((Taxi) nodeAct.getItem()).compareTo(newTaxi)==0)
				{
					break;
				}
				nodeAct = nodeAct.getNext();
			}
			
			tail.setNextNode(node);
			tail = node;
			size++;
		}
	}
	
	public int size()
	{
		return size;
	}
	
	public Node getFirst()
	{
		return head;
	}
}
