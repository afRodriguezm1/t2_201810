package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.Item;
import model.data_structures.LinkedList;
import model.data_structures.List;
import model.data_structures.Node;

import com.google.gson.*;

public class TaxiTripsManager<T> implements ITaxiTripsManager 
{
	private List listaNodos;

	public void loadServices (String serviceFile) 
	{
		listaNodos = new List();
		try 
		{
			BufferedReader bfr = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length;i++)
			{
				Node<T> nodo = new Node(item[i]);
				listaNodos.add(nodo);
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("---------------Ha ocurrido un problema---------------\n");
		}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) 
	{
		LinkedList<Taxi> listaTaxis = new List();
		Node nodoAct = listaNodos.getFirst();
		while(nodoAct != null)
		{
			String compania = (((Item)nodoAct.getItem()).getCompany() == null)? "independientes": ((Item)nodoAct.getItem()).getCompany() ;
			if(compania.equals(company))
			{
				Taxi newTaxi = new Taxi(((Item)nodoAct.getItem()).getTaxi_id(), ((Item)nodoAct.getItem()).getCompany());
				listaTaxis.addTaxi( new Node(newTaxi));
			}
			nodoAct = nodoAct.getNext();
		}
		return listaTaxis;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) 
	{
		LinkedList<Service> listaServices = new List();
		Node nodoAct = listaNodos.getFirst();
		while(nodoAct != null)
		{
			int area = ((Item)nodoAct.getItem()).getDropoff_community_area();
			if(area == communityArea)
			{
				Service servicio = new Service(((Item)nodoAct.getItem()).getTrip_id(), ((Item)nodoAct.getItem()).getTaxi_id(), ((Item)nodoAct.getItem()).getTrip_seconds(), ((Item)nodoAct.getItem()).getTrip_miles(), ((Item)nodoAct.getItem()).getTrip_total());
				listaServices.add( new Node(servicio));
			}
			nodoAct = nodoAct.getNext();
		}

		return listaServices;
	}


}
