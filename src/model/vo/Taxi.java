package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxiId;
	private String company;
	private Taxi next;
	
	public Taxi(String pId, String pComp)
	{
		taxiId = pId;
		company = pComp;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() 
	{
		// TODO Auto-generated method stub
		return taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() 
	{
		// TODO Auto-generated method stub
		return company;
	}
	
	public Taxi getNext()
	{
		return next;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		if(o.getTaxiId().equals(taxiId))
		{
			return 0;
		}
		return 1;
	}	
}
