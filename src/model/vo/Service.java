package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> 
{
	private String tripId;
	
	private String taxiId;
	
	private int tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	public Service(String pTrip, String pTaxi, int pSeconds, double pMiles, double pTotal)
	{
		tripId = pTrip;
		taxiId = pTaxi;
		tripSeconds = pSeconds;
		tripMiles = pMiles;
		tripTotal = pTotal;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return tripTotal;
	}

	@Override
	public int compareTo(Service o) 
	{
		return 0;
	}
}