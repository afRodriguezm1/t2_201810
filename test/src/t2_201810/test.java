package t2_201810;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;
import static org.junit.Assert.*;
import model.data_structures.Item;
import model.data_structures.List;
import model.data_structures.Node;

public class test
{
	private String serviceFile = "./data/test/taxi-trips-wrvz-psew-subset-test.json";
	
	private List listaNodos = new List();
	
	public void setupEscenario1()
	{
		try 
		{
			BufferedReader bfr = new BufferedReader( new FileReader(""));
			fail("Debi� generar error");
		} 
		catch (FileNotFoundException e) 
		{
		}
		try
		{
			BufferedReader bfr = new BufferedReader( new FileReader(serviceFile));
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Item[] item = gson.fromJson(bfr, Item[].class);
			for(int i = 0; i < item.length;i++)
			{
				Node nodo = new Node(item[i]);
				listaNodos.add(nodo);
			}
			
		}
		catch(Exception e)
		{
			fail("No debi� generar error");
		}
	}
	
	@Test
	public void testCargaNodos()
	{
		setupEscenario1();
		assertEquals("No se carg� correctamente los datos", 20 ,listaNodos.size());
	}
	
	@Test
	public void testGetFirts()
	{
		setupEscenario1();
		assertEquals("No retorno el primer nodo", "51b522c6a1cf950faa3cc6dc34f6446801b0a5bc54d8decccce2d2b364d09e15e751bb546f39db7f14acc6ed0e13355827bffd06f8812f47746eae70daf87fe7", ((Item)listaNodos.getFirst().getItem()).getTaxi_id());
	}
	
	@Test
	public void testGetIndex()
	{
		setupEscenario1();
		assertEquals("No retorn� el nodo esperado", "398a697d813055db99bc6a4422617b857b8f8b53ac4e4d6ba859a4bf75c8dc1e253b949dc8219dfb25c0bba0c18df6a75225d08ec17e76a81dc3700133975f8e", ((Item)listaNodos.get(4).getItem()).getTaxi_id());
	}
}
